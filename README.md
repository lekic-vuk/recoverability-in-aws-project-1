### Project 1 Description:
In this project, you will use the **AWS CloudFormation** to create **Virtual Private Clouds**. CloudFormation is an AWS service that allows you to create *infrastructure as code*.
This allows you to define the infrastructure you'd like to create in code, just like you do with software. This has the benefits of being able to share your infrastructure in a common language,
use source code control systems to version your infrastructure and allows for documenting and reviewing of infrastructure and infrastructure proposed changes.

CloudFormation allows you to use a configuration file written in a **YAML** file to automate the creation of AWS resources such as VPCs. In this project, you will use a pre-made CloudFormation template to get you started.
This will allow you to create some of the infrastructure that you'll need without spending a lot of time learning details that are beyond the scope of this course.

You can find the YAML file [at projects GitHub repo](https://github.com/udacity/nd063-c2-design-for-availability-resilience-reliability-replacement-project-starter-template/blob/master/cloudformation/vpc.yaml).